"use client";
import { Button, Form, Input } from "antd";
import React, { useState } from "react";
import { useGlobalContext } from "../contexts/useGlobalContext";
import axios from "axios";
import { ICreateUserResponse } from "../interfaces/UserInterface";
import toast from "react-hot-toast";
type FieldType = {
  name?: string;
  email?: string;
};

const UserForm = () => {
  const [isLoading, setLoading] = useState(false);
  const { setUsers } = useGlobalContext();
  const onFinish = (values: FieldType) => {
    setLoading(true);
    axios
      .post(`${process.env.NEXT_PUBLIC_BACKEND_URL}/user`, values)
      .then((response) => {
        const createUserResponse = response.data as ICreateUserResponse;
        toast.success(createUserResponse.message);
        form.resetFields();
        if (createUserResponse.metadata) {
          setUsers((prevState) => {
            return [...prevState, createUserResponse.metadata!];
          });
        }
      })
      .catch((error) => {
        toast.error(error.response.data.message);
        console.log("ERROR", error);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const [form] = Form.useForm();
  return (
    <Form
      form={form}
      name="basic"
      className="p-4"
      onFinish={onFinish}
      autoComplete="off"
    >
      <Form.Item<FieldType>
        label="Name"
        name="name"
        rules={[{ required: true, message: "Please input your name!" }]}
      >
        <Input />
      </Form.Item>

      <Form.Item<FieldType>
        label="Email"
        name="email"
        rules={[{ required: true, message: "Please input your Email!" }]}
      >
        <Input type="email" />
      </Form.Item>

      <Form.Item className="w-full">
        <Button
          type="default"
          className="w-full"
          htmlType="submit"
          loading={isLoading}
        >
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};

export default UserForm;
