"use client";
import { Button, Modal } from "antd";
import React, { useState } from "react";
import UserForm from "./UserForm";

const ModalUser: React.FC = () => {
  const [open, setOpen] = useState(false);

  const showModal = () => {
    setOpen(true);
  };

  const handleCancel = () => {
    setOpen(false);
  };

  return (
    <>
      <Button type="default" className="mb-4 float-right" onClick={showModal}>
        Create User
      </Button>
      <Modal
        title="Create User Modal"
        open={open}
        footer={false}
        onCancel={handleCancel}
      >
        <UserForm />
      </Modal>
    </>
  );
};

export default ModalUser;
