"use client";
import { Button, Popconfirm, Space, Table } from "antd";
import axios from "axios";
import React, { useEffect } from "react";
import toast from "react-hot-toast";
import { useGlobalContext } from "../contexts/useGlobalContext";
import { IUser, IUsersResponse } from "../interfaces/UserInterface";

const { Column } = Table;

const TableUser: React.FC = () => {
  const { users, setUsers } = useGlobalContext();
  useEffect(() => {
    axios
      .get(`${process.env.NEXT_PUBLIC_BACKEND_URL}/user`)
      .then((response) => {
        const userData = response.data as IUsersResponse;
        setUsers(userData.metadata ?? []);
      });
  }, [setUsers]);

  const onDelete = (record: IUser) => {
    if (!record) return;
    axios
      .delete(`${process.env.NEXT_PUBLIC_BACKEND_URL}/user/${record.id}`)
      .then((response) => {
        toast.success(response.data.message);
        setUsers((prevState) => {
          return prevState.filter((item) => item.id != record.id);
        });
      })
      .catch((error) => {
        toast.error(error.response.data.message ?? "We got error!");
      });
  };

  return (
    <Table dataSource={users}>
      <Column title="Name" dataIndex="name" key="name" />
      <Column title="Email" dataIndex="email" key="email" />
      <Column
        title="Action"
        key="action"
        render={(_: any, record: IUser) => (
          <Space size="middle">
            <a>Update</a>
            <Popconfirm
              title="Delete the task"
              description="Are you sure to delete this task?"
              onConfirm={() => onDelete(record)}
              okText="Yes"
              cancelText="No"
            >
              <Button danger>Delete</Button>
            </Popconfirm>
          </Space>
        )}
      />
    </Table>
  );
};

export default TableUser;
