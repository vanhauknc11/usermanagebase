"use client";
import {
  Dispatch,
  SetStateAction,
  createContext,
  useContext,
  useState,
} from "react";
import { IUser } from "../interfaces/UserInterface";

interface IGlobalContext {
  users: IUser[];
  setUsers: Dispatch<SetStateAction<IUser[]>>;
}

const globalContextAbs: IGlobalContext = {
  users: [],
  setUsers: () => {},
};

const GlobalContext = createContext<IGlobalContext>(globalContextAbs);

export const useGlobalContext = (): IGlobalContext => {
  return useContext(GlobalContext);
};

const GlobalContextProvider = ({ children }: { children: React.ReactNode }) => {
  const [users, setUsers] = useState<IUser[]>([]);

  const globalContextData = {
    users,
    setUsers,
  };
  return (
    <GlobalContext.Provider value={globalContextData}>
      {children}
    </GlobalContext.Provider>
  );
};

export default GlobalContextProvider;
