import { Button } from "antd";
import TableUser from "./components/TableUser";
import ModalUser from "./components/ModalUser";
import { Toaster } from "react-hot-toast";

export default function Home() {
  return (
    <div className="flex justify-center h-screen items-center">
      <div className="container">
        <h1 className="text-center text-2xl font-bold text-gray-600">
          User Manage
        </h1>
        <div className="mt-10">
          <ModalUser />
          <TableUser />
        </div>
      </div>
      <div>
        <Toaster />
      </div>
    </div>
  );
}
