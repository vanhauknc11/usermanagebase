export interface ISuccessResponse {
  statusCode: number;
  message: string;
  metadata?: any;
}

export interface IUser {
  id: number;
  name: string;
  email: string;
}

export interface IUsersResponse extends ISuccessResponse {
  metadata?: IUser[];
}

export interface ICreateUserResponse extends ISuccessResponse {
  metadata?: IUser;
}
